/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    BE_URL: process.env.BE_URL,
    VERCEL_ACCESS_TOKEN: process.env.VERCEL_ACCESS_TOKEN,
  },
};

export default nextConfig;
