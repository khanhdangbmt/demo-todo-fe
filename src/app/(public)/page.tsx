"use client";

import { Popconfirm } from "@/components/ui/Popconfirm";
import { Spinner } from "@/components/ui/Spinner";
import { Popover, PopoverTrigger } from "@/components/ui/popover";
import {
  Tooltip,
  TooltipContent,
  TooltipTrigger,
} from "@/components/ui/tooltip";
import { pushNotify } from "@/lib/toast";
import { Todo } from "@/models/todo.model";
import {
  createTodo,
  getListTodos,
  triggerCompleteTodo,
  deleteTodo,
} from "@/services/todo.service";
import { PopoverContent } from "@radix-ui/react-popover";
import { useMutation, useQuery } from "@tanstack/react-query";
import { Suspense, useMemo, useState } from "react";

export default function Component() {
  const [newTodo, setNewTodo] = useState("");
  const [isLoading, setLoading] = useState<boolean>(false);

  const listTodos = useQuery({
    queryFn: () => getListTodos(),
    queryKey: ["query list todos"],
    enabled: true,
    refetchOnWindowFocus: false,
  });

  const triggerCompleteTodoMutation = useMutation({
    mutationFn: (todoId: string) => triggerCompleteTodo(todoId),
    mutationKey: ["trigger-complete-todo"],
    onSettled: () => {
      listTodos.refetch();
      setLoading(false);
    },
  });

  const createTodoMutation = useMutation({
    mutationFn: (todo: Partial<Todo>) => createTodo(todo),
    onSuccess: () => {
      listTodos.refetch();
    },
    onSettled: () => {
      setLoading(false);
    },
    mutationKey: ["create-todo"],
  });

  const deleteTodoMutation = useMutation({
    mutationFn: (todoId: string) => deleteTodo(todoId),
    onSuccess: () => {
      listTodos.refetch();
      pushNotify("Todo deleted successfully");
    },
    onSettled: () => {
      setLoading(false);
    },
    mutationKey: ["delete-todo"],
  });

  const todos = useMemo(() => {
    return listTodos?.data?.data || [];
  }, [listTodos]);

  const addTodo = () => {
    if (!!newTodo?.trim()) {
      setLoading(true);
      createTodoMutation.mutate({
        name: newTodo,
      });
      setNewTodo("");
    }
  };
  const toggleTodo = (id: any) => {
    setLoading(true);
    triggerCompleteTodoMutation.mutate(id);
  };

  const handleDeleteTodo = (id: any) => {
    setLoading(true);
    deleteTodoMutation.mutate(id);
  };

  return (
    <div className="flex flex-col items-center justify-center h-screen bg-gray-100 dark:bg-gray-900">
      <div className="w-full max-w-md p-6 bg-white rounded-lg shadow-md dark:bg-gray-800 ">
        <h1 className="text-2xl font-bold mb-4 text-center text-gray-800 dark:text-gray-200">
          Todo App - Khanhdang
        </h1>
        <div className="flex mb-4">
          <input
            type="text"
            placeholder="Add a new todo..."
            value={newTodo}
            onChange={(e) => setNewTodo(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                addTodo();
              }
            }}
            className="flex-1 px-4 py-2 rounded-l-md bg-gray-100 dark:bg-gray-700 dark:text-gray-200 focus:outline-none focus:ring-2 focus:ring-blue-500 dark:focus:ring-blue-400"
          />
          <button
            onClick={addTodo}
            className="px-4 py-2 bg-blue-500 text-white rounded-r-md hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 dark:bg-blue-400 dark:hover:bg-blue-500 dark:focus:ring-blue-400"
          >
            Add
          </button>
        </div>
        <Spinner isLoading={isLoading || listTodos.isLoading}>
          <ul className="space-y-2 overflow-auto max-h-[600px] min-h-[100px]">
            {todos.map((todo: any, index: number) => (
              <li
                key={"td" + todo.id + index}
                className={`flex items-center justify-between px-4 py-2 rounded-md ${
                  todo.completed
                    ? "bg-gray-200 line-through text-gray-500 dark:bg-gray-700 dark:text-gray-400"
                    : "bg-white dark:bg-gray-800 dark:text-gray-200"
                }`}
              >
                <Popover>
                  <div className="flex items-center w-full break-all">
                    <input
                      type="checkbox"
                      checked={todo.completed}
                      onChange={() => toggleTodo(todo._id)}
                      className="mr-2 rounded-md focus:ring-2 focus:ring-blue-500 dark:focus:ring-blue-400"
                    />

                    <Tooltip>
                      <TooltipTrigger asChild>
                        <span className="w-full text-1-line">{todo.name}</span>
                      </TooltipTrigger>
                      <TooltipContent
                        className="max-w-[50%] flex justify-center items-center"
                        align="center"
                      >
                        <p>{todo.name}</p>
                      </TooltipContent>
                    </Tooltip>
                  </div>
                </Popover>
                <Popconfirm onOk={() => handleDeleteTodo(todo._id)}>
                  <button className="text-gray-500 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-300 focus:outline-none">
                    <TrashIcon className="w-5 h-5" />
                  </button>
                </Popconfirm>
              </li>
            ))}
          </ul>
        </Spinner>
      </div>
    </div>
  );
}

function TrashIcon(props: any) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M3 6h18" />
      <path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6" />
      <path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2" />
    </svg>
  );
}
