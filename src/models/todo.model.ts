export interface Todo {
  name: string;
  completed: boolean;
  isDeleted?: boolean;
  createdAt?: string;
  updatedAt?: string;
}
