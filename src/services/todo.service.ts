import { IResponse } from "@/lib/response";
import { Todo } from "@/models/todo.model";

const headers = new Headers({
  Authorization: `Bearer ${process.env.VERCEL_ACCESS_TOKEN}`,
  Accept: "application/json",
  "Content-Type": "application/json",
});

export const getListTodos = async (): Promise<IResponse<Todo[]>> => {
  const result = await fetch(process.env.BE_URL + "/todo/list", {
    method: "GET",
    headers,
  });
  return await result.json();
};

export const triggerCompleteTodo = async (todoId: string) => {
  const result = await fetch(process.env.BE_URL + `/todo/${todoId}`, {
    method: "PUT",
    headers,
  });

  return await result.json();
};

export const createTodo = async (todo: any): Promise<IResponse<Todo>> => {
  const result = await fetch(process.env.BE_URL + "/todo/", {
    body: JSON.stringify(todo),
    method: "POST",
    headers,
  });
  return await result.json();
};

export const deleteTodo = async (todoId: string) => {
  const result = await fetch(process.env.BE_URL + `/todo/${todoId}`, {
    method: "DELETE",
  });

  return await result.json();
};
