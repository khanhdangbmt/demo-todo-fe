import { useCallback, useState } from "react";

export function debounceFn<T extends Function>(cb: T, wait = 300) {
  let h: any;
  let callable = (...args: any) => {
    clearTimeout(h);
    h = setTimeout(() => cb(...args), wait);
  };
  return callable;
}

const useDebounce = (obj: any = null, wait: number = 300) => {
  const [state, setState] = useState(obj);

  const setDebouncedState = (_val: any) => {
    debounce(_val);
  };

  const debounce = useCallback(
    debounceFn((_prop: string) => {
      setState(_prop);
    }, wait),
    []
  );

  return [state, setDebouncedState];
};

export default useDebounce;
